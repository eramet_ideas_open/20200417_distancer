import os
import cv2
import numpy as np

from distancer.config import Config
from distancer.infrastructure.camera import Camera


class YoloBodyDetector(Camera):
    """Full body detector

    Attributes
    ----------
    all_faces : list of int quadruplets
        list of detected faces
    """
    CLASSES_PATH = os.path.join(Config().MODEL_DIR, "yolov3.txt")
    WEIGHT_PATH = os.path.join(Config().MODEL_DIR, "yolov3.weights")
    CONFIG_PATH = os.path.join(Config().MODEL_DIR, "yolov3.cfg")

    def __init__(self, cam_id=0):
        """

        Parameters
        ----------
        cam_id : integer
            camera id
        """
        super().__init__(cam_id)
        self.classes = None
        with open(self.CLASSES_PATH, 'r') as f:
            self.classes = [line.strip() for line in f.readlines()]
        self.model = cv2.dnn.readNet(self.WEIGHT_PATH, self.CONFIG_PATH)


    def detect_full_body(self, show=False):
        """Detect full bodies on current frame

        Parameters
        ----------
        show: boolean
            select if image will be shown
        """
        #Pretreatment of image
        blob = cv2.dnn.blobFromImage(self.frame, 0.00392,
                                     (224,224), (0,0,0), True, crop=False)
        #Model input
        self.model.setInput(blob)
        #Run inference through the network and gather predictions
        preds = self.model.forward(self._get_output_layers())

        #Building boxes
        confidences = []
        boxes = []
        for pred in preds:
            for detection in pred:
                scores = detection[5:]
                class_id = np.argmax(scores)
                label = str(self.classes[class_id])
                if label == "person":
                    confidence = scores[class_id]
                    if confidence > 0.5:
                        center_x = int(detection[0] * self.frame.shape[1])
                        center_y = int(detection[1] * self.frame.shape[0])
                        w = int(detection[2] * self.frame.shape[1])
                        h = int(detection[3] * self.frame.shape[0])
                        x = center_x - w / 2
                        y = center_y - h / 2
                        confidences.append(float(confidence))
                        boxes.append([x, y, w, h])

        #Addin boxes to list
        self.all_faces = []
        idx = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
        for i in idx:
            i = i[0]
            box = boxes[i]
            self.all_faces.append((box[0], box[1], box[2], box[3]))
            if show:
                x = box[0]
                y = box[1]
                w = box[2]
                h = box[3]
                cv2.rectangle(self.frame, (round(x), round(y)),
                                           (round(x + w), round(y + h)),
                                           (255, 255, 0), 2)

        if show:
            cv2.imshow("object detection", self.frame)

    def _get_output_layers(self):
        """Function to get output layer names
        """
        layer_names = self.model.getLayerNames()
        output_layers = [layer_names[i[0] - 1] for i in self.model.getUnconnectedOutLayers()]

        return output_layers


if __name__ == "__main__":
    ybd = YoloBodyDetector("D:\\Projets\\Distancer\\20200417_distancer\\data\\raw\\marche.mp4")
    while True:
        ybd.get_frame()
        ybd.detect_full_body(show=True)
        if cv2.waitKey(1) == 27:
            break
