import cv2


class Camera():
    """Camera

    Attributes
    ----------
    capture : :obj:
        open cv video capture
    frame ; 2d numpy array of int triplets
        open cv image
    """
    def __init__(self, cam_id=0):
        """

        Parameters
        ----------
        cam_id : integer
            camera id
        """
        self.capture = cv2.VideoCapture(cam_id)

    def get_frame(self, show=False):
        """Get frame
        """
        ret, self.frame = self.capture.read()
        if show:
            cv2.imshow("video", self.frame)


    def __del__(self):
        """
        """
        self.capture.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    c = Camera()
    while True:
        c.get_frame()
        if cv2.waitKey(1) == 27:
            break
