import os
import cv2
import math

from distancer.config import Config
from distancer.domain.yolo_body_detector import YoloBodyDetector

class DistanceAlarm(YoloBodyDetector):
    """Turn on distance detector

    Attributes
    ----------
    pixel_per_m : string
        pixel per m, with an ax + by + c format (Example:
        "40 + 0.3*x" + 0.1*y)
    a : float
        a coefficient of the distance equation
    b : float
        b coefficient of the distance equation
    c : float
        c coefficient of the distance equation
    """
    MIN_ACCEPTABLE_DIST = Config().MIN_ACCEPTABLE_DIST
    DISTANCE_INI_PATH = os.path.join(Config().MODEL_DIR, "distance_config.ini")


    def __init__(self, cam_id=0):
        """

        Parameters
        ----------
        cam_id : integer, optional
            camera id
        """
        super().__init__(cam_id)
        self.load_distance_config()

    def load_distance_config(self):
        """Load distance configuration
        """
        with open(self.DISTANCE_INI_PATH, 'r') as f:
            self.pixel_per_m = f.read().replace('\n', '')
        self.build_dist_coeffs()

    def build_dist_coeffs(self):
        """Build distance coefficients
        """
        pixel_per_m = str(self.pixel_per_m).replace(" ", "")
        pixel_per_m = pixel_per_m.replace("+", "|")
        pixel_per_m = pixel_per_m.replace("-", "|-")
        pixel_per_m = pixel_per_m.split("|")
        self.a = 0
        self.b = 0
        self.c = 0
        for idx in range(3):
            try:
                if ("x" in pixel_per_m[idx]):
                    pixel_per_m[idx] = pixel_per_m[idx].replace("*", "").replace("x", "")
                    self.a = float(pixel_per_m[idx])
                elif ("y" in pixel_per_m[idx]):
                    pixel_per_m[idx] = pixel_per_m[idx].replace("*", "").replace("y", "")
                    self.b = float(pixel_per_m[idx])
                else:
                    self.c = float(pixel_per_m[idx])
            except IndexError:
                pass

    def detect_when_too_close(self):
        """Detect when 2 people are 2 close
        """
        #Checking closeness
        too_close_faces = []
        for face1 in self.all_faces:
            x1 = (face1[0] + face1[0] + face1[2])/2
            y1 = (face1[1] + face1[1] + face1[3])/2
            for face2 in self.all_faces:
                x2 = (face2[0] + face2[0] + face2[2])/2
                y2 = (face2[1] + face2[1] + face2[3])/2
                dist = math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
                if not self.check_ok_dist(x1, y1, x2, y2) and dist > 5:
                    too_close_faces += [face1, face2]
        #Drawing
        img_rect = self.frame.copy()
        for (x, y, w, h) in self.all_faces:
            img_rect = cv2.rectangle(img_rect, (round(x), round(y)),
                                         (round(x + w), round(y + h)),
                                         (255, 0, 0), 2)
        for (x, y, w, h) in too_close_faces:
            img_rect = cv2.rectangle(img_rect, (round(x), round(y)),
                                         (round(x + w), round(y + h)),
                                         (0, 0, 255), 2)
        cv2.imshow("video", img_rect)

    def check_ok_dist(self, x1, y1, x2, y2):
        """Check if the distance between 2 faces is OK

        Parameters
        ----------
        x1 : float
            x location of face 1
        y1 : float
            y location of face 1
        x2 : float
            x location of face 2
        y2 : float
            y location of face 2

        Returns
        -------
        dist_ok : boolean
            True if the distance is OK
        """
        #Minimum distances
        centre_x = abs(x1 + x2)/2
        centre_y = abs(y1 + y2)/2
        pixel_per_m = self.a*centre_x + self.b*centre_y + self.c

        #Checking distance condition
        dist = math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
        #print(dist, pixel_per_m*self.MIN_ACCEPTABLE_DIST )
        if pixel_per_m*self.MIN_ACCEPTABLE_DIST > dist :
            return False
        else:
            return True


if __name__ == "__main__":
    da = DistanceAlarm()
    while True:
        da.get_frame()
        da.detect_full_body()
        da.detect_when_too_close()
        if cv2.waitKey(1) == 27:
            break
