import cv2

from distancer.domain.distance_alarm import DistanceAlarm

da = DistanceAlarm()
while True:
    da.get_frame()
    da.detect_full_body()
    da.detect_when_too_close()
    if cv2.waitKey(1) == 27:
        break
