# Social distancer

An open-source social distance analyser, light enough so it  can be used on any
kind of camera (even your laptop webcam)

## Requirements
* python 3.7.7 (anaconda distribution recommended)
* opencv 4.1.1
* numpy 1.16.3

## Installation
* Download the YOLO model weights from
https://pjreddie.com/media/files/yolov3.weights
and put the downloaded file in the model folder;
* Modify the config.py file so the path to the model folder can be found;
* Add the project repository to your PYTHONPATH environment variable.

## Calibrate distance
* Run the run_calibration.py file in the application folder with the
following command:
`python run_calibration.py`
Click space to save your distance configuration when suitable.

## Run
* Run the run_distance_alarm.py file in the application folder with the
following command:
`python run_distance_alarm.py`



Additional information on the used pretrained YOLO models can be found in:
https://github.com/arunponnusamy/object-detection-opencv
