import cv2

from distancer.domain.calibrator import Calibrator

pixel_per_m = "80 + 0.1*y" #Exemple "40 + 0.3*x + 0.1*y"
c = Calibrator(pixel_per_m=pixel_per_m)
while True:
    c.get_frame()
    c.test()
    if cv2.waitKey(1) == 32:
        c.save(pixel_per_m)
    if cv2.waitKey(1) == 27:
        break
