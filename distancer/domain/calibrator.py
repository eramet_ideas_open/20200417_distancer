import os
import cv2
import numpy as np

from distancer.config import Config
from distancer.infrastructure.camera import Camera


class Calibrator(Camera):
    """A tool to calibrate on your distances on your camera

    Attributes
    ----------
    text : string
        text on image
    pixel_per_m : string
        pixel per m, with an ax + by + c format (Example:
        "40 + 0.3*x" + 0.1*y)
    a : float
        a coefficient of the distance equation
    b : float
        b coefficient of the distance equation
    c : float
        c coefficient of the distance equation
    """
    DISTANCE_INI_PATH = os.path.join(Config().MODEL_DIR, "distance_config.ini")
    MIN_ACCEPTABLE_DIST = Config().MIN_ACCEPTABLE_DIST

    def __init__(self, pixel_per_m, cam_id=0):
        """

        Parameters
        ----------
        pixel_per_m : string, optional
            pixel per m, with an ax + by + c format (Example:
            "40 + 0.3*x" + 0.1*y)
        cam_id : integer
            camera id
        """
        super().__init__(cam_id)
        self.pixel_per_m = pixel_per_m
        self.text = "Distance equation (pixel per m): '" \
                        + str(self.pixel_per_m) \
                        + "' - A yellow line represents " \
                        + str(self.MIN_ACCEPTABLE_DIST) + "m"
        self.build_dist_coeffs()

    def build_dist_coeffs(self,):
        """Build distance coefficients
        """
        pixel_per_m = str(self.pixel_per_m).replace(" ", "")
        pixel_per_m = pixel_per_m.replace("+", "|")
        pixel_per_m = pixel_per_m.replace("-", "|-")
        pixel_per_m = pixel_per_m.split("|")
        self.a = 0
        self.b = 0
        self.c = 0
        for idx in range(3):
            try:
                if ("x" in pixel_per_m[idx]):
                    pixel_per_m[idx] = pixel_per_m[idx].replace("*", "").replace("x", "")
                    self.a = float(pixel_per_m[idx])
                elif ("y" in pixel_per_m[idx]):
                    pixel_per_m[idx] = pixel_per_m[idx].replace("*", "").replace("y", "")
                    self.b = float(pixel_per_m[idx])
                else:
                    self.c = float(pixel_per_m[idx])
            except IndexError:
                pass

    def test(self):
        """Test distance coefficients
        """
        img = self.frame.copy()
        h, w = img.shape[:2]
        xs = [int(w/7), int(3*w/7), int(5*w/7)]
        ys = [int(h/7), int(3*h/7), int(5*h/7)]
        offset = 0
        for x in xs:
            for y in ys:
                img = cv2.line(img, (x, y + offset),
                               (x + int(self.a*x + self.b*(y + offset) + self.c),
                                    y + offset),
                                (0, 255, 255), 4)
            offset += 10
        img = cv2.putText(img, self.text, (20, h - 50),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 1,
                            cv2.LINE_AA)
        cv2.imshow("video", img)

    def save(self, pixel_per_m):
        """Save pixel per m formula in distance_config.ini file

        Parameters
        ----------
        pixel_per_m : string
            pixel per m, with an ax + by + c format (Example:
            "40 + 0.3*x" + 0.1*y)
        """
        with open(self.DISTANCE_INI_PATH, "w") as f:
            f.write(pixel_per_m)
        print("Distance calibration saved.")


if __name__ == "__main__":
    pixel_per_m = "80 + 0.1*y" #Exemple "40 + 0.3*x + 0.1*y"
    c = Calibrator(pixel_per_m=pixel_per_m)
    while True:
        c.get_frame()
        c.test()
        if cv2.waitKey(1) == 32:
            c.save(pixel_per_m)
        if cv2.waitKey(1) == 27:
            break
